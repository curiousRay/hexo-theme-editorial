hexo-theme-editorial
===
An elegant hexo blog theme, migrated from html5up.net

## 配置方法
#### 1. 根目录下的`_config.yml`

- Site
控制站点的标题
```
title: yoursite.com
subtitle: subtitle
```
- Extensions
```
theme: editorial
```
- 搜索
如需使用搜索功能，务请加上
```
search:
  path: search.xml
```

#### 2. 主题目录下的`_config.yml`
- Sidebar
此五项可选择性地显示，或自由调换顺序。
```
widgets:
- search
- menu
- projects
- friends
- nav-footer
```
`search` -> 搜索框

`menu` -> 目录

`projects` -> 项目链接

`friends` -> 友链

`nav-footer` -> 版权信息等

- 分类目录和存档目录计数
```
showCategoryCount: true
showArchiveCount: true
```
- 项目链接
```
projects:
-
 title: hexo-theme-editoral
 description: A hexo theme.
 picurl: images/bg.png
 projurl: https://www.hexo.io
```
`title` -> 项目标题
`description` -> 描述文字
`picurl` -> 封面图片地址
`projurl` -> 项目地址

- 友链
```
description: 'they are my friends'
links:
 sitex: https://www.hexo.io
 sitey: https://www.hexo.io
```
- 链接图标
```
icons:
 twitter: https://www.hexo.io
 gitlab: https://www.hexo.io
 telegram: https://www.hexo.io
```
- 杂项
```
favicon: images/favicon.png
android_chrome_color: '#000'
```
`androd_chrome_color` -> 控制手机版Chrome浏览器标题栏颜色
